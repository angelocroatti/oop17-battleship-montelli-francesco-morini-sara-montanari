package view.navy_setup;

import view.interfaces.BasicView;
import view.interfaces.ConfermableView;

/**
 * Interface for the {@link NavyBuider} setup.
 */
public interface NavySetupUI extends BasicView, ConfermableView {
}
